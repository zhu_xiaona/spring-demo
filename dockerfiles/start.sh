#!/bin/bash

# jvm参数配置
export GC_STRATEGY="-XX:+UseConcMarkSweepGC -XX:CMSInitiatingOccupancyFraction=70 -XX:+UseCMSInitiatingOccupancyOnly -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=${LOG_PATH}/heapdump-%t.hprof -Xss2048k -XX:NewRatio=2 -XX:+PrintGCDetails -XX:+PrintGCDateStamps -Xloggc:${LOG_PATH}/gc-%t.log"

# start java service
cd /workspace
JAR_FILE=`ls | grep .jar$`
$JAVA_HOME/bin/java ${GC_STRATEGY}  -jar  $JAR_FILE
