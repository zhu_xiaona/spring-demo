package com.example.springdemo;

import com.fasterxml.jackson.databind.util.JSONPObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * description
 *
 * @author yangchengvita
 * @date 2021-08-18
 */
@RestController
public class HealthCheckController {

    @GetMapping("/healthcheck")
    public JSONPObject check() {
        JSONPObject jsonpObject = new JSONPObject("hello", "world");
        return jsonpObject;
    }
}
